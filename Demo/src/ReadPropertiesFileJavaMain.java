import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.Stream;
/*
 * @author Arpit Mandliya
 */
public class ReadPropertiesFileJavaMain {

	 public static void main(String args[]) throws IOException
 {
	
	
	File f = new File("src\\demo.txt");
	FileOutputStream fos = new FileOutputStream(f);
	DataOutputStream dos = new DataOutputStream(fos);
	dos.writeUTF("Hello");
	
	FileInputStream fis =new FileInputStream(f);
	DataInputStream dis = new DataInputStream(fis);
	String str = dis.readUTF();
	System.out.println(str);
 }
 
  
}
